class Article < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :has_seens
  validates :title,
            presence: true,
            length: {minimum: 5},
            uniqueness: true
  validates :text,
            presence: true,
            length: {minimum: 20}
end
