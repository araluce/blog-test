class HasSeen < ApplicationRecord
  belongs_to :user
  belongs_to :article
  before_create :set_date_to_now
  validates_uniqueness_of :user_id, :scope => :article_id

  def set_date_to_now
    self.date = Time.now
  end
end
