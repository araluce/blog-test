class ArticlesController < ApplicationController

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!, except: [:create, :new]

  #GET article
  def index
    @articles = Article.all
  end

  #GET articles/:id
  def show
    @article = Article.find(params[:id])
    visto = HasSeen.new
    visto.user_id = current_user.id
    visto.article_id = params[:id]
    visto.save
  end

  #GET /articles/new
  def new
    @article = Article.new
  end

  def create
    @article = current_user.articles.new(article_params)

    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to articles_path
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name])
  end

  private
  def article_params
    params.require(:article).permit(:title, :text)
  end
end
