class ChangeDateTypeInHasSeen < ActiveRecord::Migration[5.1]
  def change
    change_column :has_seens, :date, :timestamp
  end
end
