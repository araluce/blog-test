class CreateHasSeens < ActiveRecord::Migration[5.1]
  def change
    create_table :has_seens do |t|
      t.references :user, foreign_key: true
      t.references :article, foreign_key: true
      t.date :date

      t.timestamps
    end
  end
end
