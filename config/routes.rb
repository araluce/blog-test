Rails.application.routes.draw do

  devise_for :admins
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users , path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register', edit: 'settings' }
  # Si solo no queremos que se generen rutas para eliminar articulos
  #     resources :articles, except: [:delete]
  # Si solo queremos que se creen rutas solo para crear y para mostrar
  #     resources :articles, only: [:create, :show]

  # Genera una ruta para acceder a welcome/index
  # Esto ejecutará la función index del controlador welcome
  get 'welcome/index'

  # Si lo que queremos es que la raíz cargue index del controlador articles...
  root :to => "articles#index"

  # resources genera todas las acciones CRUD para artículos
  # a su vex estamos generando acciones CRUD para los comentarios de cada artículo
  resources :articles do
    resources :comments
  end

  # resources genera estos comportamientos
  #   get "articles" index
  #   post "articles" new
  #   delete "articles" destroy
  #   get "articles/:id" show
  #   get "articles/:id/edit" edit
  #   patch "articles/:id" update
  #   put "articles/:id" update

end
